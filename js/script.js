const cities = ["Chernihiv", "Mykolayiv", "Kyiv", "Irpin", "Boryspil", "Prolisky", "Hnidyn", "Odessa", "Lviv",];
const citiesList = document.querySelector('#citiesList');
const ol = document.createElement('ol');

const createList = (array, parrent = document.body) => {
  parrent.append(ol);
  ol.innerHTML = array.join('');
}

const arrFromCities = cities.map(element => `<li>${element}</li>`);

ol.innerHTML = arrFromCities.join('');

createList(arrFromCities, citiesList)

let timeToClear = 3;

let timerHeading = document.createElement('p');

citiesList.append(timerHeading);
timerHeading.innerText = (`Clear all after ${timeToClear} second`);

let timerToСlear = setInterval(() => timerHeading.innerText = (`Clear all after ${--timeToClear} second`), 1000);

setTimeout(() => { clearInterval(timerToСlear); document.body.innerText = '' }, timeToClear * 1000);




